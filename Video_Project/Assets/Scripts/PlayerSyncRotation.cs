﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

/**
    @author: GTGD
    @date: 10/10/2016

    This script shows the other player rotations on the network.

    @var syncRotation: The server-side value of the player's rotation
    @var syncCamera: The server-side value of the player camera's rotation
    @var playerTransform: The client-side value of the player's rotation
    @var cameraTransform: The client-side value of the player camera's rotation
    @var slerpRate: rate for interpolating rotation over the network
*/
public class PlayerSyncRotation : NetworkBehaviour
{
    [SyncVar]
    Quaternion syncRotation;
    [SyncVar]
    Quaternion syncCamera;

    [SerializeField]
    Transform playerTransform;
    [SerializeField]
    Transform cameraTransform;
    [SerializeField]
    float slerpRate = 15f;
    


    void FixedUpdate()
    {
        TransmitRotations();
        SlerpRotations();
    }

    /**
        SlerpRotations interpolates the other players' rotations on the network
    */
    void SlerpRotations()
    {
        if (!isLocalPlayer)
        {
            playerTransform.rotation = Quaternion.Slerp(playerTransform.rotation, syncRotation, Time.deltaTime * slerpRate);
            cameraTransform.rotation = Quaternion.Slerp(cameraTransform.rotation, syncCamera, Time.deltaTime * slerpRate);
        }
    }

    /**
        Cmd_ProvideRotationsToServer tells the server the rotations of the other players
           in the server.
    */
    [Command]
    void Cmd_ProvideRotationsToServer(Quaternion playerRot, Quaternion camRot)
    {
        syncRotation = playerRot;
        syncCamera = camRot;
    }

    /**
        TransmitRotations calls the command to the server to store the other players' rotations
           in the server.
    */
    [ClientCallback]
    void TransmitRotations()
    {
        if (isLocalPlayer)
        {
            Cmd_ProvideRotationsToServer(playerTransform.rotation, cameraTransform.rotation);
        }
    }
}
