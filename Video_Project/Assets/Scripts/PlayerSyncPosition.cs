﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

/**
    @author: GTGD
    @date: 10/7/16

    This script shows the other player movements on the network.

    @var syncPos: The server-side value of the player's position
    @var playerTransform: the client-side value of the player's position
    @var lerpRate: rate for interpolating position over the network.
*/
public class PlayerSyncPosition : NetworkBehaviour 
{
    [SyncVar]
    Vector3 syncPos;

    [SerializeField]
    Transform playerTransform;

    [SerializeField]
    float lerpRate = 15f;

    void FixedUpdate()
    {
        TransmitPosition();
        LerpPosition();
    }

    /**
        LerpPosition interpolates the other players' positions on the network.
    */
    void LerpPosition()
    {
        if (!isLocalPlayer)
        {
            playerTransform.position = Vector3.Lerp(playerTransform.position, syncPos, Time.deltaTime * lerpRate);
        }
    }

    /**
        Cmd_ProvidePositionToServer tells the server the positions of the other players
           in the server.
    */
    [Command]
    void Cmd_ProvidePositionToServer(Vector3 pos)
    {
        syncPos = pos;
    }

    /**
        TransmitPosition calls the command to the server to store the other players' positions
           in the server.
    */
    [ClientCallback]
    void TransmitPosition()
    {
        if (isLocalPlayer)
        {
            Cmd_ProvidePositionToServer(playerTransform.position);
        }
    }
}
