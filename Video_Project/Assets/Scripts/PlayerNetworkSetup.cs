﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

/**
    @author: GTGD
    @date: 10/7/16

    This script sets up a player character when entering the network scene.

    @pre: A scene camera must exist; the player prefab must have its character controller, first-person controller,
             and in its children the camera and audio listener components disabled.
*/
public class PlayerNetworkSetup : NetworkBehaviour {

    //[SerializeField]
    //Camera FPSCharacterCam;

    //[SerializeField]
    //AudioListener FPSListener;
    
	void Start ()
    {
        if (isLocalPlayer)
        {
            GameObject.Find("Scene Camera").SetActive(false);
            GetComponent<CharacterController>().enabled = true;
            GetComponent<UnityStandardAssets.Characters.FirstPerson.FirstPersonController>().enabled = true;
            GetComponentInChildren<Camera>().enabled = true;
            GetComponentInChildren<AudioListener>().enabled = true;
            //FPSCharacterCam.enabled = true;
            //FPSListener.enabled = true;
        }
	}
}
